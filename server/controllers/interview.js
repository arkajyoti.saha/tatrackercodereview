const Candidate = require('../models/candidates');
const User = require('../models/users');
const Interview = require('../models/interview');
const jwt = require('jsonwebtoken');

exports.middleware = async(req, res, next) => {
    const accessToken = req.headers["x-access-token"];
    const { userId, exp } = await jwt.verify(accessToken, process.env.JWT_SECRET);
    if (exp < Date.now().valueOf() / 1000) { 
        return res.status(401).json({ error: "JWT token has expired" });
    } 
    const interviewer = await User.findById(userId);
    req.interviewerId = userId;
    if(interviewer.type.toString() != 'interviewer'){
        return res.status(401).json({
            message: "you are not interviewer so not allowed to update interview model"
        });
    }
    next();
}

exports.start = async (req, res, next) => {
    try{
        const intervieID = req.params.userId;
        var user;
        var flag = true;
        while(flag){
            user = await Interview.findOne({intervieID});
            setTimeout(() => {flag = false}, 20000);
        }
        // const user = await Interview.findOne({intervieID});
        setTimeout(() => {res.status(200).json({
            data: user,
            message: "interview is over"
            });}, 20000);
    }
    catch(err){
        console.log(err);
    }
}
exports.update = async (req, res, next) => {
    try {
        const userId = req.interviewerId;
        const interviewer = await User.findById(userId);
        const update = req.body;
        const interviewId = req.params.userId;
        const interview = await Interview.findOneAndUpdate({interviewId}, update)
        const candidate_id = interview.candidate_id;
        const candidate = await Candidate.findOne({candidate_id});
        candidate.interview = interview;
        console.log(candidate);
        console.log(interviewer);
        interviewer.active = true;
        candidate.status = interview.status;
        candidate.updated_at = Date.now();
        candidate.save();
        interviewer.save();
        if (!interview) return next(new Error('User does not exist'));
    //     setTimeout(() => {res.status(200).json({
    //         data: interview,
    //         message: "interview is over"
    //         });}, 10000);
        res.status(200).json({
            data: interview,
            message: "details updated"
        })
    } 
       catch (error) {
        next(error)
    }
}

exports.getUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        // console.log(userId);
        const user = await Interview.findById(userId);
        if (!user) return next(new Error('User does not exist'));
        res.status(200).json({
        data: user
        });
    } catch (error) {
        next(error)
    }
};

exports.getUsers = async (req, res, next) => {
    const users = await Interview.find({});
    console.log(users);
    res.status(200).json({
     data: users
    });
}


