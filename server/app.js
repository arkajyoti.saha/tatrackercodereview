const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const path = require('path')
const User = require('./models/users')
const UserRoutes = require('./routes/users');
const CandidateRoute = require('./routes/candidate');
const interviewSlotRoute = require('./routes/interviewSlot');
const interviewRoute = require('./routes/interview');
const cron = require('node-cron');
//requireing the models
const candidates = require('./models/candidates');
const user = require('./models/users');
const interview = require('./models/interview');
const interview_slot = require('./models/interviewSlot');
const { createDecipheriv } = require('crypto');
//////////////////////////////////////////////////////////////
require("dotenv").config({
    path: path.join(__dirname, "../.env")
});
 
const app = express();
app.use(express.json());
 
const PORT = process.env.PORT || 3000;

const dbUI = "mongodb://localhost:27017/Rough";
mongoose.connect(dbUI).then(() => {
    console.log("connection succesfull");
}).catch((err)=>{
    console.log("connectin failed");
});

app.use(bodyParser.urlencoded({ extended: true }));
  
//#######################..............................CRON Function................................................####################
// const CRON = require('./jobSchedullar/corn');
// let canMap = new Map();
// candidates.find({status:'created'},(err,candidate1) => {
//     if(err){
//         console.log(err);
//     }
//     else{
//         for(var i =0;i<candidate1.length;i++){
//             const slot = candidate1[i].slots;
//             const canID = candidate1[i]._id;
//             canMap.set(canID, slot);
//         }
//     }
// })


// let CanVis = new Set();
// // CanMap.set(1,"arka");
// const job2 = function(){
// let CanMap = new Map();   //canID, canModel
// let SlotMapCan = new Map();
// let interviewerMap = new Map(); //interviewerID, interviewer
// let interviewerSlotMap = new Map(); //interviewerID, slots
// let matchedSlots = new Map();
// let slotID = new Map();
//     candidates.find({status:'created'}, (err, candiadate1) =>{
//         if(err){
//             console.log(err);
//         }
//         else{
//             interview_slot.find({}, (err,interview_slot1) => {
//                 if(err){
//                     console.log(err);
//                 }
//                 else{
//                     for(var i = 0;i<interview_slot1.length;i++){
//                         const interviewerID = interview_slot1[i].interviwer_id;
//                         const slot = interview_slot1[i].slots;
//                         interviewerSlotMap.set(interviewerID,slot);
//                     }
//                 }
//             })
//             for(var i=0;i<candiadate1.length;i++){
//                 if(!CanVis.has(candiadate1[i]._id.toString())){
//                     CanMap.set(candiadate1[i]._id, candiadate1[i]);
//                     SlotMapCan.set(candiadate1[i]._id,candiadate1[i].slots);
//                 }
//             }
//             user.find({type: 'interviewer'}, (err, interviewer1) => {
//                 if(err){
//                     console.log(err);
//                 }
//                 else{
//                     for(var i=0;i<interviewer1.length;i++){
//                         interviewerMap.set(interviewer1[i]._id, interviewer1[i]);
//                     }
//                     interview_slot.find({}, (err,interview_slot1) => {
//                         if(err){
//                             console.log(err);
//                         }
//                         else{
//                             for(var i = 0;i<interview_slot1.length;i++){
//                                 const interviewerID = interview_slot1[i].interviwer_id;
//                                 const slot = interview_slot1[i].slots;
//                                 interviewerSlotMap.set(interviewerID,slot);
//                             }
//                             var flag = false;
//                             for(let [canID,canslots] of SlotMapCan){
//                                 for(let [intID,intslots] of interviewerSlotMap){
//                                     for(let canSlot of canslots){
//                                         for(let intSlot of intslots){
//                                             if(canSlot.start.toString() === intSlot.start.toString() && canSlot.end.toString() === intSlot.end.toString()){
//                                                 const interview01 = new interview({});
//                                                 interview01.candidate_id = canID;
//                                                 interview01.interviewer_id = intID;
//                                                 interview01.save();
//                                                 CanVis.add(canID.toString());
//                                                 console.log(" scheduled");
//                                                 flag = true;
//                                             }
//                                             if(flag){
//                                                 break;
//                                             }
//                                         }
//                                         if(flag){
//                                             break;
//                                         }
//                                     }if(flag){
//                                         break;
//                                     }
//                                 }
//                                 if(flag){
//                                     break;
//                                 }
//                             }
//                             for(var i=0;i<candiadate1.length;i++){
//                                 if(CanVis.has(candiadate1[i]._id.toString())){
//                                     candiadate1[i].status = "scheduled";
//                                     candiadate1[i].save();
//                                 }
//                             // console.log(candiadate1[i]._id);
//                             }
//                         }
//                     })
//                 }
//             })
//         }
        
//         // for(var keys of )
//     })
//     console.log("i ran");
// };
// console.log(CanMap.keys());
const job3 = function(){
    var canMap = new Map();
    var intMap = new Map();
    candidates.find({status: 'created'}, (err,candiadate1) => { //master slots
        if(err){
            console.log(err);
        }
        else{
            interview_slot.find({}, (err, interview_slot1) => {
                // console.log(interview_slot1.length);
                for(var j =0 ;j<interview_slot1.length;j++){
                    const interviewSlot = interview_slot1[j].slots;
                    console.log(interviewSlot);
                    for(var I=0 ;I<interviewSlot.length;I++){
                        const start = interviewSlot[I].start;
                        const end = interviewSlot[I].end;
                        intMap.set(start,end);
                    }
                }
            });
            for(var k =0 ;k<candiadate1.length;k++){
                const candidateSlot = candiadate1[k].slots;
                // console.log(candidateSlot);
                for(var J = 0;J<candidateSlot.length;J++){
                    const start = candidateSlot[J].start;
                    const end = candidateSlot[J].end;
                    canMap.set(start,end);
                }
                for (const [key, value] of canMap) {
                    if(intMap.has(key) && value === intMap.get(key)){
                        const canID = candiadate1[k];
                        const interview01 = new interview({});
                        interview01.candidate_id = canID;
                        interview01.interviewer_id = intervierID;
                        candiadate1[k].status = "scheduled";
                        console.log(candiadate1[k].status);
                        candiadate1[k].save();
                        interview01.save();
                    }
                }
            }
            for(const [key,val] of intMap){
                console.log("interviewer");
                console.log(key," : ",val);
            }
            for(const [key,val] of canMap){
                console.log("candidate");
                console.log(key," : ",val);
            }
            
        }
    });
    console.log("i ran");
}


//////////////////

// const job = function(){
//     var canMap = new Map();
//     var intMap = new Map();
//     candidates.find({status: 'created'}, (err,candiadate1) => { //master slots
//         if(err){
//             console.log(err);
//         }
//         else{
//             user.find({type: 'interviewer'}, (err, interviewer1)=>{
//                 const flag = {};
//                 for(var i = 0; i < interviewer1.length;i++){
//                     const intervierID = interviewer1[i];
//                     interview_slot.find({interviewer_id: intervierID}, (err, interview_slot1) => {
//                         for(var j =0 ;j<interview_slot1.length;j++){
//                             const interviewSlot = interview_slot1[j].slots;
//                             for(var k =0 ;k<candiadate1.length;k++){
//                                 const candidateSlot = candiadate1[k].slots;
//                                 // console.log(candidateSlot);
//                                 for(var J = 0;J<candidateSlot.length;J++){
//                                     const start = candidateSlot[J].start.toString();
//                                     const end = candidateSlot[J].end.toString();
//                                     // const startObj = {start: candidateSlot[J].start.toTimeString()};
//                                     canMap.set(start,end);
//                                 }
//                                 for(var I=0 ;I<interviewSlot.length;I++){
//                                     const start = interviewSlot[I].start.toString();
//                                     const end = interviewSlot[I].end.toString();
//                                     intMap.set(start,end);
//                                 }
//                                 for (const [key, value] of canMap) {
//                                     if(intMap.has(key) && value === intMap.get(key) && candiadate1[k].status === "created" && intervierID.active){
//                                         intervierID.active = false;
//                                         const canID = candiadate1[k];
//                                         const interview01 = new interview({});
//                                         interview01.candidate_id = canID;
//                                         interview01.interviewer_id = intervierID;
//                                         candiadate1[k].status = "scheduled";
//                                         console.log(candiadate1[k].status);
//                                         intervierID.save();
//                                         candiadate1[k].save();
//                                         interview01.save();
//                                     }
//                                 }
//                                 // for(var J = 0;J<candidateSlot.length;J++){
//                                 //     for(var I=0 ;I<interviewSlot.length;I++){
//                                 //         if(interviewSlot[I].start.toString() === candidateSlot[J].start.toString() && interviewSlot[I].end.toString() === candidateSlot[J].end.toString() && candiadate1[k].status === "created" && intervierID.active){
//                                 //             intervierID.active = false;
//                                 //             const canID = candiadate1[k];
//                                 //             const interview01 = new interview({});
//                                 //             interview01.candidate_id = canID;
//                                 //             interview01.interviewer_id = intervierID;
//                                 //             candiadate1[k].status = "scheduled";
//                                 //             console.log(candiadate1[k].status);
//                                 //             intervierID.save();
//                                 //             candiadate1[k].save();
//                                 //             interview01.save();
//                                 //         }
//                                 //     }
//                                 // }
//                             }
//                         }
//                     })
//                 }
//             });
//         }
//     });
//     console.log("i ran");
// }

cron.schedule("*/0.5 * * * *", job3);
   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   app.use('/ta', UserRoutes); // i am the ta
   app.use('/createCandidate',CandidateRoute); // want to crate candiadate
   app.use('/interviewSlot', interviewSlotRoute); // changes in the interview slot
   app.use('/interview', interviewRoute); // interview model

   module.exports = app;