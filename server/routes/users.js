const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
console.log("i am inside routes/users");
 
router.post('/createTa', userController.create);
 
router.post('/login', userController.login);

router.post('/createInterviewer',userController.middleware,userController.onboard);

router.post('/logout', userController.logout);
 
router.get('/user/:userId', userController.allowIfLoggedin, userController.getUser);
 
router.get('/users',userController.middleware ,userController.getUsers);
 
 
module.exports = router;
