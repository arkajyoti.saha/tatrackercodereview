const express = require('express');
const router = express.Router();
const userController = require('../controllers/interview');
console.log('i am inside router/interview');
 
router.post('/start/:userId',userController.middleware,userController.start);
router.post('/update/:userId',userController.middleware,userController.update);
router.get('/view/:userId', userController.getUser);
router.get('/getInterviews', userController.getUsers);

module.exports = router;